Low Karma Remover Bot
=====================

OAuth Version for Python3
-------------------------

The low_karma_remover_bot_3.py is built for python3 and uses OAuth instead of 
the old Praw login as that will soon be deprecated. Instead of python2 this 
requires python3, praw and the [prawoauthutil-2](https://github.com/SmBe19/praw-OAuth2Util) built by [/u/SmBe19](https://www.reddit.com/user/SmBe19).

To get this setup install praw and praw-oauth2util:

    pip install praw
    pip install praw-oauth2util

Then follow the instructions for setting up a reddit app [here](https://github.com/SmBe19/praw-OAuth2Util/blob/master/OAuth2Util/README.md#reddit-config).

And then add your app_key and app_secret to the oauth-template.txt file, and rename it to oauth.txt. 


Setup for Python2 version (will not work in August 2015)
--------------------------------------------------------

This script requires python 2.7 and praw.

To install praw type in your command line:

    pip install praw


Config
------

Add the reddit username and password of the bot account to the REDDIT_USERNAME and REDDIT_PASS variables.
Then add the subreddit name that the bot account is a moderator of.

Then you have 2 options to control, the THRESHOLD variable which will be the number you want posts to be less than
to be removed. And the REPLY variable which is what the bot will comment on the post to let the user know it's been
removed. 
