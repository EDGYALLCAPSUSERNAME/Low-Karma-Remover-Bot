import praw
import time
import OAuth2Util

SUBREDDIT_NAME = ''

THRESHOLD = 0

REPLY = """
Your post has been removed because it has fallen under the point
threshold. If you think this is a mistake, please message the moderators.
"""


def find_posts_to_remove(sub):
    submissions = sub.get_new()
    for submission in submissions:
        if submission.score < THRESHOLD:
            remove_post(submission)


def remove_post(submission):
    post_comment(submission)

    print("Removing post...")
    submission.remove()


def post_comment(submission):
    print("Commenting on post...")
    submission.add_comment(REPLY)


def main():
    r = praw.Reddit(user_agent='Low_Karma_Remover v2.0 /u/EDGYALLCAPSUSERNAME')
    o = OAuth2Util.OAuth2Util(r, print_log=True)
    o.refresh()
    sub = r.get_subreddit(SUBREDDIT_NAME)

    while True:
        # refresh the token if need be
        o.refresh()
        find_posts_to_remove(sub)
        # wait 5 minutes to not go over limit
        print("Sleeping...")
        time.sleep(300)

if __name__ == '__main__':
    main()
