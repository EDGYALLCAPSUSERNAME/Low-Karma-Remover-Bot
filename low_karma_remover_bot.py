import praw
import time

REDDIT_USERNAME = ''
REDDIT_PASS = ''
SUBREDDIT_NAME = ''

THRESHOLD = 0

REPLY = """
Your post has been removed because it has fallen under the point
threshold. If you think this is a mistake, please message the moderators.
"""


def find_posts_to_remove(sub):
    while True:
        submissions = sub.get_new(limit=100)
        for submission in submissions:
            if submission.score < THRESHOLD:
                remove_post(submission)

        # wait 5 minutes to not go over limit
        print "Sleeping..."
        time.sleep(300)


def remove_post(submission):
    post_comment(submission)

    print "Removing post..."
    submission.remove()


def post_comment(submission):
    print "Commenting on post..."
    submission.add_comment(REPLY)


def main():
    r = praw.Reddit(user_agent='Low_Karma_Remover v1.0 /u/EDGYALLCAPSUSERNAME')

    print "Logging in..."
    r.login(REDDIT_USERNAME, REDDIT_PASS, disable_warning=True)
    sub = r.get_subreddit(SUBREDDIT_NAME)

    find_posts_to_remove(sub)

if __name__ == '__main__':
    main()
